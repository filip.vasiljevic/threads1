#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <ctype.h>

#define RING_SIZE  (16)
#define SLEEPING_TIME (400000)

char getch(void);

/*  Kruzni bafer - FIFO. */
struct RingBuffer
{
    unsigned int  tail;
    unsigned int  head;
    unsigned char data[RING_SIZE];
};

/* Operacije za rad sa kruznim baferom. */
char ringBufGetChar (struct RingBuffer *apBuffer)
{
    int index;
    index = apBuffer->head;
    apBuffer->head = (apBuffer->head + 1) % RING_SIZE;
    return apBuffer->data[index];
}

void ringBufPutChar (struct RingBuffer *apBuffer, const char c)
{
    apBuffer->data[apBuffer->tail] = c;
    apBuffer->tail = (apBuffer->tail + 1) % RING_SIZE;
}

static struct RingBuffer ring;
static struct RingBuffer outBuff;

static sem_t inEmpty;
static sem_t inFull;
static sem_t outEmpty;
static sem_t outFull;
static sem_t semFinishSignal;

static pthread_mutex_t bufferAccess;
static pthread_mutex_t outAccess;

/* Funkcija programske niti proizvodjaca. */
void* producer (void *param)
{
    char c;

    while (1)
    {
        if (sem_trywait(&semFinishSignal) == 0)
        {
            break;
        }

        if (sem_trywait(&inEmpty) == 0)
        {

            c = getch();

            if (c == 'q' || c == 'Q')
            {
                sem_post(&semFinishSignal);
                sem_post(&semFinishSignal);
                sem_post(&semFinishSignal);
            }

            pthread_mutex_lock(&bufferAccess);
            ringBufPutChar(&ring, c);
            pthread_mutex_unlock(&bufferAccess);

            sem_post(&inFull);
        }
    }

    return 0;
}

void* processer(void *param)
{
    char c;

    while(1)
    {
        if(sem_trywait(&semFinishSignal) == 0)
        {
	    break;
	}
        
	if(sem_trywait(&inFull) == 0)
	{
	    pthread_mutex_lock(&bufferAccess);
	    c = ringBufGetChar(&ring);
	    c = toupper(c);
	    pthread_mutex_unlock(&bufferAccess);
	    
	    pthread_mutex_lock(&outAccess);
	    ringBufPutChar(&outBuff, c);
	    pthread_mutex_unlock(&outAccess);
	    sem_post(&inEmpty);
	    sem_post(&outFull);
	}
    }

    return 0;
}



/* Funkcija programske niti potrosaca.*/
void* consumer (void *param)
{
    char c;

    printf("Znakovi preuzeti iz kruznog bafera:\n");

    while (1)
    {
        if (sem_trywait(&semFinishSignal) == 0)
        {
            break;
        }

        if (sem_trywait(&outFull) == 0)
        {
            pthread_mutex_lock(&outAccess);
	    c = ringBufGetChar(&outBuff);
	    pthread_mutex_unlock(&outAccess);

            printf("%c", c);
            fflush(stdout);

            usleep(SLEEPING_TIME);

            sem_post(&outEmpty);
        }
    }

    return 0;
}

int main (void)
{
    pthread_t hProducer;
    pthread_t hConsumer;
    pthread_t hProcesser;

    sem_init(&inEmpty, 0, RING_SIZE);
    sem_init(&inFull, 0, 0);
    sem_init(&semFinishSignal, 0, 0);
   
    sem_init(&outFull, 0, 0);
    sem_init(&outEmpty, 0, 0);

    pthread_mutex_init(&bufferAccess, NULL);
    pthread_mutex_init(&outAccess, NULL);
    pthread_create(&hProducer, NULL, producer, 0);
    pthread_create(&hConsumer, NULL, consumer, 0);
    pthread_create(&hProcesser, NULL, processer, 0);

    pthread_join(hConsumer, NULL);
    pthread_join(hProducer, NULL);
    pthread_join(hProcesser, NULL);

    sem_destroy(&inEmpty);
    sem_destroy(&inFull);
    sem_destroy(&outEmpty);
    sem_destroy(&outFull);
    sem_destroy(&semFinishSignal);
    pthread_mutex_destroy(&bufferAccess);
    pthread_mutex_destroy(&outAccess);

    printf("\n");

    return 0;
}