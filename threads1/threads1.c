#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

static int turn = 1;
static pthread_mutex_t mutex;
static pthread_cond_t cv;

void* print(void *pParam)
{
    int i = 0;

    int id = *(int*)pParam;

    while(i < 100)
    {
        pthread_mutex_lock(&mutex);
        
        while(turn != id)
            pthread_cond_wait(&cv, &mutex);

        printf("%d", id);
        fflush(stdout);
        i++;
        turn++;
        if(turn > 3)
            turn = 1;
            
        pthread_cond_broadcast(&cv);
        pthread_mutex_unlock(&mutex);
    }

    return 0;
}

int main (void)
{
    pthread_t hPrint1;
    pthread_t hPrint2;
    pthread_t hPrint3;
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cv, NULL);

    int id1 = 1;
    int id2 = 2;
    int id3 = 3;

    pthread_create(&hPrint1, NULL, print, &id1);
    pthread_create(&hPrint2, NULL, print, &id2);
    pthread_create(&hPrint3, NULL, print, &id3);

    pthread_join(hPrint1, 0);
    pthread_join(hPrint2, 0);
    pthread_join(hPrint3, 0);

    printf("\n");

    return 0;
}
